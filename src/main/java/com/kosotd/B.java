package com.kosotd;

import kernel.annotations.Inject;
import kernel.annotations.Dependency;
import kernel.annotations.InjectValue;
import kernel.db.IRepository;

@Dependency
public class B {

    @Inject
    private A aInst;

    @Inject
    private C c;

    @Inject
    private TestIntf intf;

    @Inject
    private IRepository repository;

    public B(@InjectValue("hello") String data, @InjectValue("${xVal}") int x){
        this.x = x;
        this.data = data;
    }

    int x;
    String data;

    public void aMethod(){
        aInst.method();
        intf.method();
        System.out.println("-----------");
        repository.getObjects(A.class, null).stream().forEach(a -> {
            System.out.println(a.id + " " + a.data);
        });
        System.out.println("-----------");
        c.printX();
    }

    public void method(){
        System.out.println("method " + data + " " + x + " of B class");
    }
}
