package com.kosotd;

import kernel.annotations.Dependency;
import kernel.annotations.Inject;
import kernel.annotations.InjectValue;

/**
 * Created by kosotd on 01.05.2017.
 */

@Dependency
public class C {

    @InjectValue("345")
    int x;
    @InjectValue("${xVal}")
    int y;

    public C(@Inject A a, @Inject D d){
        System.out.println("init C ----------");
        System.out.println(a);
        d.method();
        System.out.println("init C ----------");
    }

    public void printX(){
        System.out.println("x : " + x + " y : " + y);
    }
}
