package com.kosotd;

import kernel.annotations.Dependency;
import kernel.annotations.Inject;
import kernel.annotations.InjectValue;

/**
 * Created by kosotd on 01.05.2017.
 */

@Dependency
public class D {
    public void method(){
        System.out.println("method D");
    }

    public D(@InjectValue("qwe") String s){

    }
}
