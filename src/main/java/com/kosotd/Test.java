package com.kosotd;

import kernel.annotations.Dependency;

/**
 * Created by kosotd on 30.04.2017.
 */

@Dependency
public class Test implements TestIntf {
    @Override
    public void method() {
        System.out.println("test method");
    }
}
