package com.kosotd;

import kernel.annotations.Column;
import kernel.annotations.Dependency;
import kernel.annotations.Entity;
import kernel.annotations.Id;

import java.lang.reflect.Field;

@Entity
@Dependency
public class A {

    @Id
    @Column("id")
    Long id = Long.valueOf((int)(Math.random() * 1000f));

    @Column("data")
    String data = "hello";

    public A(){

    }

    public A(Long id, String data){
        this.id = id;
        this.data = data;
    }

    @Override
    public String toString() {
        return "id: " + id + "; data: " + data;
    }

    public void method(){
        System.out.println("method of A class");
    }

}
