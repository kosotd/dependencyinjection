package com.kosotd;

import kernel.ApplicationContext;

import kernel.db.CouldNotCreateRepositoryException;
import kernel.db.IRepository;

public class Main {
    public static void main(String[] args) throws CouldNotCreateRepositoryException {
        ApplicationContext applicationContext = new ApplicationContext(new String[] {"com.kosotd", "kernel.db"});
        B b = applicationContext.getDependency(B.class);
        b.method();
        b.aMethod();

        A a = new A(1l, "hello");
        IRepository repository = applicationContext.getDependency(IRepository.class);
        clearRepository(repository, A.class);
        repository.addObject(a);
        repository.commit();
        repository.getObjects(A.class, null).stream().forEach(System.out::println);
    }

    private static <T> void clearRepository(IRepository repository, Class<T> clazz){
        repository.getObjects(clazz, null).stream().forEach(obj -> {
            repository.removeObject(obj);
        });
        repository.commit();
    }
}
