package kernel.db;

import kernel.annotations.Column;
import kernel.annotations.Dependency;
import kernel.annotations.Id;
import kernel.annotations.InjectValue;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;

@Dependency
public class H2Repository implements IRepository {
    private Connection connection;

    public H2Repository(@InjectValue("${db_url}") String url) throws CouldNotCreateRepositoryException {
        boolean err = false;
        String errString = "Невозможно подключиться базе данных";
        connection = null;
        try {
            Class.forName("org.h2.Driver");
            connection = DriverManager.getConnection(url);
            if (connection != null)
                if (!connection.isClosed()) {
                    connection.setAutoCommit(false);
                    return;
                }
            err = true;
        } catch (ClassNotFoundException ex) {
            errString = "Не найден файл базы данных h2.jar";
            err = true;
        } catch (SQLException ex) {
            err = true;
        } finally {
            if (err)
                throw new CouldNotCreateRepositoryException(errString);
        }
    }

    private String getSqlType(Class<?> clazz) {
        if (clazz == String.class){
            return "varchar(250)";
        } else if ((clazz == int.class) || (clazz == Integer.class)){
            return "integer";
        } else if ((clazz == boolean.class) || (clazz == Boolean.class)){
            return "boolean";
        }
        throw new IllegalStateException("unknown sql type");
    }

    private <T> void createTable(T object) throws SQLException {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("create table if not exists ");
        stringBuilder.append(object.getClass().getSimpleName());
        stringBuilder.append("_table (");
        int addedFieldsCount = 0;
        for (Field field : object.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(Column.class)) {
                Column ann = field.getAnnotation(Column.class);
                if (addedFieldsCount > 0)
                    stringBuilder.append(", ");
                String colName = "".equals(ann.value()) ? field.getName() : ann.value();
                stringBuilder.append(colName);
                stringBuilder.append(" ");
                if (field.isAnnotationPresent(Id.class))
                    stringBuilder.append("bigint identity");
                else
                    stringBuilder.append(getSqlType(field.getType()));
                addedFieldsCount++;
            }
        }
        stringBuilder.append(")");
        try (Statement stmt = connection.createStatement()) {
            stmt.execute(stringBuilder.toString());
        }
    }

    private <T> String buildDeleteQuery(T object) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("delete from ");
        stringBuilder.append(object.getClass().getSimpleName());
        stringBuilder.append("_table where ");
        for (Field field : object.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(Column.class)) {
                if (field.isAnnotationPresent(Id.class))
                    stringBuilder.append(field.getName());
                stringBuilder.append("=?");
                break;
            }
        }
        return stringBuilder.toString();
    }

    private <T> String buildInsertQuery(T object) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("insert into ");
        stringBuilder.append(object.getClass().getSimpleName());
        stringBuilder.append("_table (");
        int addedFieldsCount = 0;
        for (Field field : object.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(Column.class)) {
                Column ann = field.getAnnotation(Column.class);
                if (field.isAnnotationPresent(Id.class))
                    continue;
                if (addedFieldsCount > 0)
                    stringBuilder.append(", ");
                String colName = "".equals(ann.value()) ? field.getName() : ann.value();
                stringBuilder.append(colName);
                stringBuilder.append(" ");
                addedFieldsCount++;
            }

        }
        stringBuilder.append(") values(");
        for (int i = 0; i < addedFieldsCount; ++i) {
            if (i > 0)
                stringBuilder.append(", ");
            stringBuilder.append("?");
        }
        stringBuilder.append(")");
        return stringBuilder.toString();
    }

    private <T> String buildUpdateQuery(T object) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("update ");
        stringBuilder.append(object.getClass().getSimpleName());
        stringBuilder.append("_table set ");
        int addedFieldsCount = 0;
        String uniqueFieldName = "";
        for (Field field : object.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(Column.class)) {
                Column ann = field.getAnnotation(Column.class);
                if (field.isAnnotationPresent(Id.class)) {
                    uniqueFieldName = field.getName();
                    continue;
                }
                if (addedFieldsCount > 0)
                    stringBuilder.append(", ");
                String colName = "".equals(ann.value()) ? field.getName() : ann.value();
                stringBuilder.append(colName);
                stringBuilder.append("=?");
                addedFieldsCount++;
            }
        }
        stringBuilder.append(" where ");
        stringBuilder.append(uniqueFieldName);
        stringBuilder.append("=?");
        return stringBuilder.toString();
    }

    private <T> void deleteRecord(T object) throws SQLException {
        try (PreparedStatement deleteStatement = connection.prepareStatement(buildDeleteQuery(object))) {
            for (Field field : object.getClass().getDeclaredFields()) {
                if (field.isAnnotationPresent(Column.class)) {
                    if (field.isAnnotationPresent(Id.class)) {
                        field.setAccessible(true);
                        deleteStatement.setObject(1, field.get(object));
                        break;
                    }
                }
            }
            deleteStatement.addBatch();
            deleteStatement.executeBatch();
        } catch (IllegalAccessException e) {
        }
    }

    private <T> void insertRecord(T object) throws SQLException {
        try (PreparedStatement addStatement = connection.prepareStatement(buildInsertQuery(object))) {
            int cnt = 1;
            for (Field field : object.getClass().getDeclaredFields()) {
                if (field.isAnnotationPresent(Column.class)) {
                    if (field.isAnnotationPresent(Id.class))
                        continue;
                    field.setAccessible(true);
                    addStatement.setObject(cnt++, field.get(object));
                }
            }
            addStatement.addBatch();
            addStatement.executeBatch();
        } catch (IllegalAccessException e) {
        }
    }

    private <T> void updateRecord(T object) throws SQLException {
        try (PreparedStatement updateStatement = connection.prepareStatement(buildUpdateQuery(object))) {
            int cnt = 1;
            Field[] fields = object.getClass().getDeclaredFields();
            int uniqueFieldNum = -1;
            for (int i = 0; i < fields.length; ++i) {
                Field field = fields[i];
                if (field.isAnnotationPresent(Column.class)) {
                    if (field.isAnnotationPresent(Id.class)) {
                        uniqueFieldNum = i;
                        continue;
                    }
                    field.setAccessible(true);
                    updateStatement.setObject(cnt++, field.get(object));
                }
            }
            fields[uniqueFieldNum].setAccessible(true);
            updateStatement.setObject(cnt++, fields[uniqueFieldNum].get(object));
            updateStatement.addBatch();
            updateStatement.executeBatch();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public <T> void addObject(T object) {
        try {
            createTable(object);
        } catch (SQLException ex) {
            Logger.getLogger(H2Repository.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            insertRecord(object);
        } catch (SQLException ex) {
            Logger.getLogger(H2Repository.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public <T> void updateObject(T object) {
        try {
            createTable(object);
        } catch (SQLException ex) {
            Logger.getLogger(H2Repository.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            updateRecord(object);
        } catch (SQLException ex) {
            Logger.getLogger(H2Repository.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void rollback() {
        try {
            connection.rollback();
        } catch (SQLException ex) {
            Logger.getLogger(H2Repository.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void commit() {
        try {
            connection.commit();
        } catch (SQLException ex) {
            Logger.getLogger(H2Repository.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public <T> void removeObject(T object) {
        try {
            createTable(object);
        } catch (SQLException ex) {
            Logger.getLogger(H2Repository.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            deleteRecord(object);
        } catch (SQLException ex) {
            Logger.getLogger(H2Repository.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private <T> String buildSelectQuery(Class<T> type) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("select * from ");
        stringBuilder.append(type.getSimpleName());
        stringBuilder.append("_table");
        return stringBuilder.toString();
    }

    private <T> Constructor<T> getConstructor(Class<T> type, Class[] parametrTypes) {
        try {
            return type.getConstructor(parametrTypes);
        } catch (NoSuchMethodException ex) {
            for (Constructor<?> c : type.getConstructors()) {
                if (c.getParameterCount() == parametrTypes.length)
                    return (Constructor<T>) c;
            }
        }
        return null;
    }

    @Override
    public <T> List<T> getObjects(Class<T> type, Predicate<T> filter) {
        List<T> result = new ArrayList<T>();
        try (PreparedStatement ps = connection.prepareStatement(buildSelectQuery(type))) {
            try (ResultSet rs = ps.executeQuery()) {
                ResultSetMetaData md = rs.getMetaData();
                Object[] parametrValues = new Object[md.getColumnCount()];
                Class[] parametrTypes = new Class[md.getColumnCount()];
                while (rs.next()) {
                    for (int col = 0; col < md.getColumnCount(); col++) {
                        parametrValues[col] = rs.getObject(col + 1);
                        parametrTypes[col] = null;
                        if (parametrValues[col] != null)
                            parametrTypes[col] = parametrValues[col].getClass();
                    }
                    Constructor<T> constructor = getConstructor(type, parametrTypes);
                    T element = constructor.newInstance(parametrValues);
                    if (filter != null) {
                        if (filter.test(element))
                            result.add(element);
                    } else
                        result.add(element);
                }
            } catch (SecurityException | InstantiationException |
                    IllegalAccessException | IllegalArgumentException |
                    InvocationTargetException ex) {
                Logger.getLogger(H2Repository.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
        }
        return result;
    }
}
