package kernel.db;

public class CouldNotCreateRepositoryException extends Exception {

    public CouldNotCreateRepositoryException() {
    }

    public CouldNotCreateRepositoryException(String msg) {
        super(msg);
    }
}
