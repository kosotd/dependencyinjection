package kernel.db;

import java.util.List;
import java.util.function.Predicate;

public interface IRepository {
    <T> void addObject(T object);
    <T> void removeObject(T object);
    <T> void updateObject(T object);
    <T> List<T> getObjects(Class<T> type, Predicate<T> filter);
    void commit();
    void rollback();
}
