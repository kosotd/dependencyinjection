package kernel.annotations;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by kosotd on 30.04.2017.
 */

@Retention(value= RetentionPolicy.RUNTIME)
public @interface InjectValue {
    String value();
}
