package kernel.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by User on 25.04.2017.
 */

@Retention(value= RetentionPolicy.RUNTIME)
public @interface Dependency {
}
