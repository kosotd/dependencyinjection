package kernel;

import kernel.annotations.Inject;
import kernel.annotations.Dependency;
import kernel.annotations.InjectValue;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ApplicationContext {

    private Map<Class<?>, Object> dependencies = new HashMap<>();

    public ApplicationContext(String basePackage){
        List<Class<?>> classes = findClasses(basePackage);
        addDependencies(classes);
        injectDependencies(classes);
    }

    public ApplicationContext(String[] basePackages){
        List<Class<?>> classes = new ArrayList<>();
        for (String basePackage : basePackages)
            classes.addAll(findClasses(basePackage));
        addDependencies(classes);
        injectDependencies(classes);
    }

    public <T> T getDependency(Class<T> clazz)  {
        return (T) getDependencyInternal(clazz);
    }

    private void addDependencies(List<Class<?>> classes) {
        for (Class<?> clazz : classes)
            if (clazz.isAnnotationPresent(Dependency.class))
                if (!dependencies.containsKey(clazz))
                    dependencies.put(clazz, instanceDependency(clazz));

    }

    private <T> T instanceDependency(Class<T> clazz) {
        Constructor[] constructors = clazz.getDeclaredConstructors();
        for (int j = 0; j < constructors.length; ++j) {
            Constructor constructor = constructors[j];
            Annotation[][] annotations = constructor.getParameterAnnotations();
            Class<?>[] paramClasses = constructor.getParameterTypes();
            Object[] paramValues = new Object[paramClasses.length];
            boolean ready = true;
            for (int i = 0; i < paramClasses.length; ++i) {
                Annotation annotation;
                if ((annotation = paramIsAnnotationPresent(annotations[i], InjectValue.class)) != null) {
                    InjectValue ann = (InjectValue) annotation;
                    paramValues[i] = castAnnotationValueToRequiredType(ann.value(), paramClasses[i]);
                } else if ((annotation = paramIsAnnotationPresent(annotations[i], Inject.class)) != null) {
                    try {
                        paramValues[i] = getDependencyInternal(paramClasses[i]);
                    } catch (RuntimeException ex){
                        dependencies.put(paramClasses[i], instanceDependency(paramClasses[i]));
                        paramValues[i] = getDependencyInternal(paramClasses[i]);
                    }
                } else {
                    ready = false;
                    break;
                }
            }
            if (ready)
                try {
                    return (T) constructor.newInstance(paramValues);
                } catch (Exception e) {
                    throw new RuntimeException("Error when creating class with name " + clazz.getName());
                }
        }
        throw new RuntimeException("Not found suitable constructor for class " + clazz.getName());
    }

    private void injectDependencies(List<Class<?>> classes) {
        for (Class<?> clazz : classes)
            if (clazz.isAnnotationPresent(Dependency.class)) {
                Field[] fields = clazz.getDeclaredFields();
                for (Field field : fields) {
                    Object o = getDependencyInternal(clazz);
                    if (field.isAnnotationPresent(Inject.class)) {
                        Class<?> fieldClass = field.getType();
                        Object obj = getDependencyInternal(fieldClass);
                        try {
                            field.setAccessible(true);
                            field.set(o, obj);
                        } catch (IllegalAccessException e) {
                        }
                    } else if (field.isAnnotationPresent(InjectValue.class)){
                        InjectValue ann = field.getAnnotation(InjectValue.class);
                        Object value = castAnnotationValueToRequiredType(ann.value(), field.getType());
                        try {
                            field.setAccessible(true);
                            field.set(o, value);
                        } catch (IllegalAccessException e) {
                        }
                    }
                }
            }
    }

    private Object getDependencyInternal(Class<?> fieldClass) {
        Object result = dependencies.get(fieldClass);
        if (result != null) return result;
        for (Map.Entry<Class<?>, Object> entry : dependencies.entrySet()){
            if (fieldClass.isAssignableFrom(entry.getKey()))
                return entry.getValue();
        }
        throw new RuntimeException("Dependency " + fieldClass.getName() + " not found");
    }

    private Object castAnnotationValueToRequiredType(String value, Class<?> componentType) {
        Pattern pattern = Pattern.compile("\\$\\{(\\w+)\\}", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(value.trim());
        if (matcher.matches()){
            value = loadValueFromProperties(matcher.group(1));
            if (value == null)
                throw new RuntimeException("Property " + matcher.group(1) + " not found in application.properties");
        }
        try {
            if (componentType == String.class) {
                return value;
            } else if ((componentType == int.class) || (componentType == Integer.class)) {
                return Integer.valueOf(value);
            } else if ((componentType == boolean.class) || (componentType == Boolean.class)) {
                return Boolean.valueOf(value);
            }
        } catch (Exception e){
            throw new RuntimeException("Value " + value + " can not be cast to type " + componentType.getName());
        }
        throw new RuntimeException("Injecting for type " + componentType.getName() + " not supported");
    }

    private String loadValueFromProperties(String propName){
        Properties prop = new Properties();
        InputStream input = null;
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource("application.properties").getFile());
            input = new FileInputStream(file);
            prop.load(input);
            return prop.getProperty(propName);
        } catch (IOException ex) {
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    private Annotation paramIsAnnotationPresent(Annotation[] annotations, Class<?> annotation){
        for (int i = 0; i < annotations.length; ++i){
            if (annotations[i].annotationType() == annotation)
                return annotations[i];
        }
        return null;
    }

    private List<Class<?>> findClasses(String packageName) {
        List<Class<?>> res = new ArrayList<>();
        File currentDirFile = new File("");
        String helper = currentDirFile.getAbsolutePath() + "\\src\\main\\java\\";
        File dir = new File(helper + packageName.replace('.', '\\')+"\\");
        File[] files = dir.listFiles();
        if (files == null) return res;
        for (int i = 0; i < files.length; i++) {
            File f = files[i];
            if (f.isDirectory()) {
                List<Class<?>> classes = findClasses(packageName+"."+f.getName());
                res.addAll(classes);
                continue;
            }
            String name = "";
            try {
                if (!f.getName().toLowerCase().endsWith(".java")) continue;
                name = packageName + "." + f.getName().substring(0, f.getName().length() - 5);
                Class cl = Class.forName(name);
                if (cl.isInterface() || (cl.getModifiers() & Modifier.ABSTRACT) > 0)
                    continue;
                res.add(cl);
            } catch (Exception e) {
                throw new RuntimeException("\n" + "Error loading a class " + name);
            }
        }
        return res;
    }

}
